/*
 * COMP30023: Computer Systems
 * Project 2
 * Author: Michael Lumley <mlumley>
 */
 
#ifndef CORE
#define CORE

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define CODE_LEN 4
#define NUM_VALID_CHARS 6
#define TRUE 1
#define FALSE 0

#endif

/*
 * Generates a random code
 */
char* genCode();

/*
 * Counts the num of valid characters in the code
 */
int numValid(char* code);

/*
 * Calculates the number correct colours in the correct positions
 */
int calcB(char* code, char* client_guess);

/*
 * Calculates the number correct colours in the incorrect positions
 */
int calcM(char* code, char* client_guess);

/*
 * Reset the locked array. To be called after calcB and calcM
 */
void resetLocked();