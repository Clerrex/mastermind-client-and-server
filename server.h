/*
 * COMP30023: Computer Systems
 * Project 2
 * Author: Michael Lumley <mlumley>
 */

#ifndef CORE
#define CORE

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define CODE_LEN 4
#define NUM_VALID_CHARS 6
#define TRUE 1
#define FALSE 0

#endif

#include "game.h"
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <time.h>
#include <pthread.h>
#include <signal.h>
#include <sys/resource.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <ctype.h>

#define MESSAGE_LEN	255
#define MAX_ATTEMPTS 10
#define IP_LEN 12

typedef struct game_instance_variables {
    int *new_sock;
    char *code;
    char *client_IP;
} game_instance_variables_t;

/*
 * Plays the game with the client
 */
void *game_instance(void * game_instance_variables);

/*
 * Handles the ctrl c signal to print a log file and quit
 */
void sig_handler(int signo);

/*
 * Gets the current system time
 */
char* getTime();

/*
 * Prints the VmRSS value to log_file
 */
void print_VmRSS();

/*
 * Prints the VmSize value to log_file
 */
void print_VmSize();