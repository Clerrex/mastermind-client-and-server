# Mastermind Client and Server #

### What is this repository for? ###

This project involves a multi-threaded server that can handle multiple concurrent requests
for the game Mastermind. As well as a client to play the game.
The server makes use of sockets using TCP to handle communication with the client

### How do I get set up? ###

Complie with the makefile and run the server with the argument:
1. Port Number
Then run the client with the arguments:
1. IP Address
2. Port Number