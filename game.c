/*
 * COMP30023: Computer Systems
 * Project 2
 * Author: Michael Lumley <mlumley>
 */
 
#include "game.h"

char valid_chars[6] = {'A', 'B', 'C', 'D', 'E', 'F'};
int locked[CODE_LEN] = {FALSE, FALSE, FALSE, FALSE};

/*
 * Generates a random code
 */
char* genCode() {
	char* code;
	code = malloc(CODE_LEN + 1);
	int i = 0;
	for(i = 0; i < CODE_LEN; i++){
		code[i] = valid_chars[rand() % NUM_VALID_CHARS];
	}
	code[CODE_LEN] = '\0';
	return code;
}

/*
 * Counts the num of valid characters in the code
 */
int numValid(char* code) {
	int num_valid = 0;
	int i, j;
	for(i = 0; i < CODE_LEN; i++){
		for(j = 0; j < NUM_VALID_CHARS; j++){
			if(code[i] == valid_chars[j]){
				num_valid++;
			}
		}
	}
	return num_valid;
}

/*
 * Calculates the number correct colours in the correct positions
 */
int calcB(char* code, char* client_guess){
	int i, b = 0;
	for(i = 0; i < CODE_LEN; i++){
		if (code[i] == client_guess[i]){
			// Lock the position as correct
			locked[i] = TRUE;
			b++;
		}
	}
	return b;
}

/*
 * Calculates the number correct colours in the incorrect positions
 */
int calcM(char* code, char* client_guess){
	int i, j, m = 0;
	for(i = 0; i < CODE_LEN; i++){
		for(j = 0; j < CODE_LEN; j++){
			// Look at unlocked positons i.e. incorrect positions
			if (code[i] == client_guess[j] && locked[i] == FALSE && locked[j] == FALSE){
				locked[i] = TRUE;
				m++;
			}
		}
	}
	return m;
}

/*
 * Reset the locked array. To be called after calcB and calcM
 */
void resetLocked(){
	memset(locked, FALSE, sizeof(int)*CODE_LEN);
}