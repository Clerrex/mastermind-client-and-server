/*
 * COMP30023: Computer Systems
 * Project 2
 * Author: Michael Lumley <mlumley>
 */

#include "client.h"

int main(int argc, char *argv[]) {
    int socket_fd, read_size;
    struct sockaddr_in server_addr;
    char guess[CODE_LEN+1];
    char server_msg[MESSAGE_LEN];
    char msg[MESSAGE_LEN];
    
    // Create Socket
    socket_fd = socket(AF_INET , SOCK_STREAM , 0);
    if (socket_fd == -1) {
        printf("Could not create socket\n");
        exit(1);
    }

    //Set up the socket
    inet_pton(AF_INET, argv[1], &(server_addr.sin_addr.s_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(atoi(argv[2]));
    
	// Connect to server
	if(connect(socket_fd, (struct sockaddr*) &server_addr, sizeof(server_addr)) < 0){
		printf("Could not connect %s\n", strerror(errno));
		exit(1);
	}

	// Read message from server
	while(read(socket_fd , server_msg , MESSAGE_LEN) > 0){

        printf("%s", server_msg);

        if(strncmp(server_msg, "FAILURE", 6) == 0 || strncmp(server_msg, "SUCCESS", 6) == 0){
            close(socket_fd);
            exit(1);
        }

        printf("Enter a code\n");
        // Read first 4 chars and disguard the rest
        scanf("%4s%*[^\n]%*c", guess);
        write(socket_fd, guess, CODE_LEN+1);

        bzero((char *) &server_msg, MESSAGE_LEN);
        bzero((char *) &guess, CODE_LEN+1);
	}

	return 0;
}