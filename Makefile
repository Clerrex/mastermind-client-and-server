all: server client

server: server.c
	gcc -o server server.c server.h game.c game.h -lpthread -lrt

client: client.c
	gcc -o client client.c client.h

clean:
	rm *.o