/*
 * COMP30023: Computer Systems
 * Project 2
 * Author: Michael Lumley <mlumley>
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <errno.h>

#define MESSAGE_LEN 255
#define CODE_LEN 4