/*
 * COMP30023: Computer Systems
 * Project 2
 * Author: Michael Lumley <mlumley>
 */

#include "server.h"

static pthread_mutex_t lock;
static char *server_IP = "0.0.0.0";
static FILE *log_file = NULL;
static int num_connected= 0;
static int num_success = 0;
static double cpu_time = 0;
static struct timespec begin, end;

 
int main(int argc , char *argv[]) {
	

	// Catch ctrl c
	signal(SIGINT, sig_handler);

	int socket_fd;
	int new_socket;
	int client_addr_len = sizeof(struct sockaddr_in);

	struct sockaddr_in server;
	struct sockaddr_in client;

	log_file = fopen("log.txt", "w");

	time_t result = time(NULL);
	
	//Create socket
	socket_fd = socket(AF_INET , SOCK_STREAM , 0);
	if (socket_fd == -1) {
		printf("Could not create socket\n");
		exit(1);
	}
	 
	//Set up the socket
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(atoi(argv[1]));
	
	//Bind
	if(bind(socket_fd,(struct sockaddr *)&server , sizeof(server)) < 0) {
		printf("Could not bind socket\n");
		exit(1);
	}
	 
	listen(socket_fd , 5);
	printf("Server running\n");

	while((new_socket = accept(socket_fd, (struct sockaddr*) &client, (socklen_t*) &client_addr_len))) {
		pthread_mutex_lock(&lock);
		fprintf(log_file, "[%s](%s)(%d) client connected\n", getTime(), inet_ntoa(client.sin_addr), new_socket);
		pthread_mutex_unlock(&lock);
		
		//Create the struct to pass in to game instance
		game_instance_variables_t *game_instance_variables = malloc(sizeof(game_instance_variables_t));
		// The new socket_FD
		game_instance_variables->new_sock = malloc(sizeof(int));
		*game_instance_variables->new_sock = new_socket;
		// Whether to use preset code or gen one
		if(argc == 3){
			game_instance_variables->code = argv[2];
		}
		else{
			game_instance_variables->code = genCode();
		}
		pthread_mutex_lock(&lock);
		fprintf(log_file, "[%s](%s) server secret = %s\n", getTime(), server_IP, game_instance_variables->code);
		pthread_mutex_unlock(&lock);
		// The clients IP
		char client_IP[IP_LEN+1];
		sprintf(client_IP, "%s", inet_ntoa(client.sin_addr));
		game_instance_variables->client_IP = strdup(client_IP);
		
		pthread_t new_thread;
		if(pthread_create(&new_thread , NULL ,  game_instance , game_instance_variables) < 0) {
			printf("Could not make thread\n");
			exit(1);
		}
		pthread_detach(new_thread);
	}
	
	if (new_socket < 0) {
		printf("Could not accept\n");
		exit(1);
	}

	return 0;
}
 
/*
 * Plays the game with the client
 */
void *game_instance(void* game_instance_variables) {
	// Start the timer
	struct timespec beginT, endT;
	clock_gettime(CLOCK_THREAD_CPUTIME_ID , &beginT);

	// Pull the params out of struct
	game_instance_variables_t* params = (game_instance_variables_t *) game_instance_variables;
	int sock = *params->new_sock;
	char* code = params->code;
	char* client_IP = params->client_IP;

	char hint[MESSAGE_LEN];
	char client_guess[CODE_LEN+1];
	char final_message[MESSAGE_LEN];

	int b = 0;
	int m = 0;
	int attempts = 0;
	int num_valid = 0;

	pthread_mutex_lock(&lock);
	num_connected++;
	pthread_mutex_unlock(&lock);

	//Send welcome message
	char *message = "Welcome to Mastermine\nTry and guess the 4 lettered code using the letters A - F. You have 10 attempts\n";
	write(sock , message , strlen(message)+1);

	//Receive a message from client
	while(read(sock , client_guess , CODE_LEN+1) > 0 ) {

		//printf("%s\n", client_guess);
		attempts++;
		num_valid = numValid(client_guess);


		pthread_mutex_lock(&lock);
		fprintf(log_file, "[%s](%s)(soc_id %d) client's guess = %s\n", getTime(), client_IP, sock, client_guess);
		pthread_mutex_unlock(&lock);

		// Valid code
		if(num_valid == CODE_LEN) {
			b = calcB(code, client_guess);
			m = calcM(code, client_guess);
			resetLocked();

			sprintf(hint, "Server's hint = [%d:%d]\n", b, m);       
			pthread_mutex_lock(&lock);
			fprintf(log_file, "[%s](%s) server's hint = %s", getTime(), server_IP, hint);
			pthread_mutex_unlock(&lock);
		}
		// Invalid code
		else{
			sprintf(hint, "INVALID\n");
			pthread_mutex_lock(&lock);
			fprintf(log_file, "[%s](%s)(soc_id %d) INVALID\n", getTime(), client_IP, sock);
			pthread_mutex_unlock(&lock);
		}

		// End states
		// Guessed correct code
		if (b == CODE_LEN){
			sprintf(final_message, "SUCCESS\nYou guess the code!!");
			pthread_mutex_lock(&lock);
			num_success++;
			fprintf(log_file, "[%s](%s)(soc_id %d) SUCCESS game over\n", getTime(), client_IP, sock);
			pthread_mutex_unlock(&lock);
		}
		// Reached max attempts
		else if (attempts >= MAX_ATTEMPTS){
			sprintf(final_message, "FAILURE\nYou failed as there are no more attempts.\nThe code was %s\n", code);
			pthread_mutex_lock(&lock);
			fprintf(log_file, "[%s](%s)(soc_id %d) FAILURE game over\n", getTime(), client_IP, sock);
			pthread_mutex_unlock(&lock);
		}
		// Game still going
		else{
			write(sock , hint , strlen(hint));
			bzero((char *) &hint, MESSAGE_LEN);
			bzero((char *) &client_guess, CODE_LEN);
			continue;
		}
		// Game finished, on next iteration wait for client to quit
		write(sock , final_message , strlen(final_message));
		bzero((char *) &final_message, MESSAGE_LEN);
	}
	
	// Close the socket
	free(params->new_sock);
	free(params->client_IP);
	free(params);

	// Calc CPU time for thread
	clock_gettime(CLOCK_THREAD_CPUTIME_ID , &endT);
	double elapsedT = endT.tv_sec - beginT.tv_sec;
	elapsedT += (endT.tv_nsec - beginT.tv_nsec) / 1000000000.0;

	pthread_mutex_lock(&lock);
	cpu_time += elapsedT;
	pthread_mutex_unlock(&lock);
	return 0;
}

/*
 * Handles the ctrl c signal to print a log file and quit
 */
void sig_handler(int signo) {
	if (signo == SIGINT){
		double elapsed;
		clock_gettime(CLOCK_PROCESS_CPUTIME_ID , &end);
		pthread_mutex_destroy(&lock);

		// Calc elapsed time
		elapsed = end.tv_sec - begin.tv_sec;
		elapsed += (end.tv_nsec - begin.tv_nsec) / 1000000000.0;

		fprintf(log_file, "[%s](%s) server closed\n", getTime(), server_IP);
		fprintf(log_file, "--------------------------------------------\n");
		fprintf(log_file, "Number of clients connected: %d\n", num_connected);
		fprintf(log_file, "Number of clients who guessed the code: %d\n", num_success);
		fprintf(log_file, "CPU time for process is: %gs\n", elapsed);
		fprintf(log_file, "Average CPU time per client thread is: %gs\n", cpu_time/(double)num_connected);
		print_VmRSS();
		print_VmSize();

		fclose(log_file);
		printf("\n");
		exit(0);
	}
}

/*
 * Gets the current system time
 */
char* getTime() {
	time_t result = time(NULL);
	char* time = asctime(localtime(&result));
	time[strlen(time)-1] = '\0';
	return time;
}

/*
 * Prints the RSS value to log_file
 */
void print_VmRSS() {
	char line[100], *p;
	FILE* f;

	f = fopen("/proc/self/status", "r");
	if(!f)
		return;
	// Search for the VmRSS entry
	while(fgets(line, 100, f)) {
		if(strncmp(line, "VmRSS:", 6) != 0)
			continue;
		// Get rid of VmRSS and whitespace
		p = line + 7;
		while(isspace(*p)) ++p;

		fprintf(log_file, "RSS size is: %s", p);
		break;
	}

	fclose(f);
}

/*
 * Prints the VmSize value to log_file
 */
void print_VmSize() {
	char line[100], *p;
	FILE* f;

	f = fopen("/proc/self/status", "r");
	if(!f)
		return;
	// Search for the VmSize entry
	while(fgets(line, 100, f)) {
		if(strncmp(line, "VmSize:", 6) != 0)
			continue;
		// Get rid of VmSize and whitespace
		p = line + 7;
		while(isspace(*p)) ++p;

		fprintf(log_file, "VmSize size is: %s", p);
		break;
	}

	fclose(f);
}